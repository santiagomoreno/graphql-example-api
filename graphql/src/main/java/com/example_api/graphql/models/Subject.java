package com.example_api.graphql.models;

import lombok.*;
import javax.persistence.*;

@Entity
@AllArgsConstructor
@Getter @Setter
@Builder
public class Subject {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    @OneToOne
    private Teacher teacher;
}
