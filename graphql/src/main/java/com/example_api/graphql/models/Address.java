package com.example_api.graphql.models;

import lombok.*;
import javax.persistence.*;

@Entity
@AllArgsConstructor
@Getter @Setter
@Builder
public class Address {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String street;
    private Integer number;
    private String neighborhood;
}
