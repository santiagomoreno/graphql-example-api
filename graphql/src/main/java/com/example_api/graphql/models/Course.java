package com.example_api.graphql.models;

import lombok.*;
import javax.persistence.*;
import java.util.Set;

@Entity
@AllArgsConstructor
@Getter @Setter
@Builder
public class Course {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Integer year;
    @OneToMany(mappedBy = "course")
    private Set<Student> students;
    @OneToMany
    private Set<Subject> subjects;


}
