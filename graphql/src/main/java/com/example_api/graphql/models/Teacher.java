package com.example_api.graphql.models;

import lombok.*;
import javax.persistence.*;

@Entity
@AllArgsConstructor
@Getter @Setter
@Builder
public class Teacher {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String surname;

}
