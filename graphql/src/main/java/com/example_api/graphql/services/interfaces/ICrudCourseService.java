package com.example_api.graphql.services.interfaces;

import com.example_api.graphql.models.Course;

public interface ICrudCourseService {
    Course createCourse(Integer year);
    Course findById(Long id);
}
