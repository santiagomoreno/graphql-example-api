package com.example_api.graphql.services;

import com.example_api.graphql.models.Address;
import com.example_api.graphql.models.Student;
import com.example_api.graphql.repositories.IAddressRepository;
import com.example_api.graphql.repositories.IStudentRepository;
import com.example_api.graphql.services.interfaces.students.IConnectStudentService;
import com.example_api.graphql.services.interfaces.students.ICrudStudentService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
@AllArgsConstructor
public class StudentService implements ICrudStudentService, IConnectStudentService {

    IStudentRepository iStudentRepository;
    IAddressRepository iAddressRepository;

    @Override
    public Student createStudent(String name, String surname, String maternalSurname, String birth) {
        return iStudentRepository.save(Student.builder()
                .name(name)
                .surname(surname)
                .maternalSurname(maternalSurname)
                .birth(LocalDate.parse(birth))
                .build());
    }

    @Override
    public Student updateStudent(Long studentId, String name, String surname, String maternalSurname, String birth) {
        Student student = iStudentRepository.findById(studentId).orElseThrow();

        if(name != null)student.setName(name);
        if(surname != null)student.setSurname(surname);
        if(maternalSurname != null)student.setMaternalSurname(maternalSurname);
        if(birth != null)student.setBirth(LocalDate.parse(birth));


        return iStudentRepository.save(student);
    }

    @Override
    public Student findById(Long id) {
        return iStudentRepository.getById(id);
    }

    @Override
    public List<Student> findAll(Integer quantity) {
        return iStudentRepository.findAll(PageRequest.of(1,quantity)).getContent();
    }

    @Override
    public Student connectStudentWithAddress(Long studentId, Long addressId) {
        Student student = iStudentRepository.findById(studentId).orElseThrow();
        Address address = iAddressRepository.findById(addressId).orElseThrow();

        student.setAddress(address);

        return iStudentRepository.save(student);
    }
}
