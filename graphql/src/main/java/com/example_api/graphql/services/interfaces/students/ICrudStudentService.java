package com.example_api.graphql.services.interfaces.students;

import com.example_api.graphql.models.Student;
import java.util.List;

public interface ICrudStudentService {
    Student createStudent(String name, String surname, String maternalSurname, String birth);
    Student updateStudent(Long studentId, String name, String surname, String maternalSurname, String birth);
    Student findById(Long id);
    List<Student> findAll(Integer quantity);
}
