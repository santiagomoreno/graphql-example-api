package com.example_api.graphql.services.interfaces;

import com.example_api.graphql.models.Address;

public interface ICrudAddressService {
    Address createAddress(String street, Integer number, String neighborhood);
}
