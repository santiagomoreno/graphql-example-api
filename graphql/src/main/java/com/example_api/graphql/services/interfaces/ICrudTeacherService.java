package com.example_api.graphql.services.interfaces;

import com.example_api.graphql.models.Teacher;

public interface ICrudTeacherService {
    Teacher createTeacher(String name, String surname);
    Teacher findById(Long id);
}
