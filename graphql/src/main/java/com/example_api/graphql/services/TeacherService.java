package com.example_api.graphql.services;

import com.example_api.graphql.models.Teacher;
import com.example_api.graphql.repositories.ITeacherRepository;
import com.example_api.graphql.services.interfaces.ICrudTeacherService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class TeacherService implements ICrudTeacherService {

    ITeacherRepository iTeacherRepository;

    @Override
    public Teacher createTeacher(String name, String surname) {
        return iTeacherRepository.save(Teacher.builder()
                .name(name)
                .surname(surname)
                .build());
    }

    @Override
    public Teacher findById(Long id) {
        return iTeacherRepository.findById(id).orElse(null);
    }
}
