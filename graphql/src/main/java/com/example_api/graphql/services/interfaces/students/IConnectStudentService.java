package com.example_api.graphql.services.interfaces.students;

import com.example_api.graphql.models.Student;

public interface IConnectStudentService {
    Student connectStudentWithAddress(Long studentId, Long addressId);

}
