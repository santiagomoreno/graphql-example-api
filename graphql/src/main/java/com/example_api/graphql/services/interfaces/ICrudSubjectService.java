package com.example_api.graphql.services.interfaces;

import com.example_api.graphql.dtos.SubjectAndTeacherDTO;
import com.example_api.graphql.models.Subject;

import java.util.List;

public interface ICrudSubjectService {
    Subject createSubject(String name, String teacherName, String teacherSurname);
    SubjectAndTeacherDTO findById(Long id);
    List<SubjectAndTeacherDTO> getAll(Integer quantity);
}
