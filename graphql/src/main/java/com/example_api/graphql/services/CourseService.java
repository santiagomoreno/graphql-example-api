package com.example_api.graphql.services;

import com.example_api.graphql.models.Course;
import com.example_api.graphql.repositories.ICourseRepository;
import com.example_api.graphql.services.interfaces.ICrudCourseService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class CourseService implements ICrudCourseService {

    ICourseRepository iCourseRepository;

    @Override
    public Course createCourse(Integer year) {
        return iCourseRepository.save(Course.builder().year(year).build());
    }

    @Override
    public Course findById(Long id) {
        return iCourseRepository.findById(id).orElse(null);
    }
}
