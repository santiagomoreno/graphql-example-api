package com.example_api.graphql.services;

import com.example_api.graphql.dtos.SubjectAndTeacherDTO;
import com.example_api.graphql.models.Subject;
import com.example_api.graphql.models.Teacher;
import com.example_api.graphql.repositories.ISubjectRepository;
import com.example_api.graphql.repositories.ITeacherRepository;
import com.example_api.graphql.services.interfaces.ICrudSubjectService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class SubjectService implements ICrudSubjectService {

    ISubjectRepository iSubjectRepository;
    ITeacherRepository iTeacherRepository;

    @Override
    public Subject createSubject(String name, String teacherName, String teacherSurname) {
        Teacher teacher = Teacher.builder().name(teacherName).surname(teacherSurname).build();
        Subject subject = Subject.builder().name(name).build();
        subject.setTeacher(teacher);

        iTeacherRepository.save(teacher);
        return iSubjectRepository.save(subject);
    }

    @Override
    public SubjectAndTeacherDTO findById(Long id) {
        Subject subject = iSubjectRepository.findById(id).orElseThrow();

        return new SubjectAndTeacherDTO(subject.getName(), subject.getTeacher().getName(), subject.getTeacher().getSurname());
    }

    @Override
    public List<SubjectAndTeacherDTO> getAll(Integer quantity) {
        List<Subject> subjects = iSubjectRepository.findAll(PageRequest.of(1, quantity)).getContent();

        return subjects.stream().map(x -> new SubjectAndTeacherDTO(
                        x.getName(),
                        x.getTeacher().getName(),
                        x.getTeacher().getSurname()))
                .collect(Collectors.toList());
    }
}
