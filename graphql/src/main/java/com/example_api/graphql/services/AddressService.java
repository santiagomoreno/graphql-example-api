package com.example_api.graphql.services;

import com.example_api.graphql.models.Address;
import com.example_api.graphql.repositories.IAddressRepository;
import com.example_api.graphql.services.interfaces.ICrudAddressService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class AddressService implements ICrudAddressService {

    IAddressRepository iAddressRepository;

    @Override
    public Address createAddress(String street, Integer number, String neighborhood) {
        return iAddressRepository.save(Address.builder()
                .street(street)
                .number(number)
                .neighborhood(neighborhood)
                .build());
    }
}
