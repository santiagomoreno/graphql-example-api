package com.example_api.graphql.dtos;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class SubjectAndTeacherDTO {
    private String name;
    private String teacherName;
    private String teacherSurname;
}
