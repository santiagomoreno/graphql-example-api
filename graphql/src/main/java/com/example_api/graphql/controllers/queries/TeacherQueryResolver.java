package com.example_api.graphql.controllers.queries;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import com.example_api.graphql.models.Teacher;
import com.example_api.graphql.services.interfaces.ICrudTeacherService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class TeacherQueryResolver implements GraphQLQueryResolver {

    private ICrudTeacherService teacherService;

    public Teacher getOneTeacher(final Long id){
        return teacherService.findById(id);
    }
}

