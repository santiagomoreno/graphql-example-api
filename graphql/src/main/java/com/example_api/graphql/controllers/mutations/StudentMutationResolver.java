package com.example_api.graphql.controllers.mutations;

import com.coxautodev.graphql.tools.GraphQLMutationResolver;
import com.example_api.graphql.models.Student;
import com.example_api.graphql.services.interfaces.students.IConnectStudentService;
import com.example_api.graphql.services.interfaces.students.ICrudStudentService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class StudentMutationResolver implements GraphQLMutationResolver {

    private ICrudStudentService iStudentService;
    private IConnectStudentService iConnectService;


    public Student createStudent(final String name, String surname, String maternalSurname, String birth){
        return iStudentService.createStudent(name, surname, maternalSurname, birth);
    }

    public Student updateStudent(final Long studentId, String name, String surname, String maternalSurname, String birth){
        return iStudentService.updateStudent(studentId,name, surname, maternalSurname, birth);
    }

    public Student connectStudentWithAddress(final Long studentId, Long addressId){
        return iConnectService.connectStudentWithAddress(studentId,addressId);
    }

}