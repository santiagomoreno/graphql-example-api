package com.example_api.graphql.controllers.queries;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import com.example_api.graphql.models.Course;
import com.example_api.graphql.services.interfaces.ICrudCourseService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class CourseQueryResolver implements GraphQLQueryResolver {

    private ICrudCourseService courseService;

    public Course getOneCourse(final Long id){
        return courseService.findById(id);
    }
}
