package com.example_api.graphql.controllers.mutations;

import com.coxautodev.graphql.tools.GraphQLMutationResolver;
import com.example_api.graphql.models.Subject;
import com.example_api.graphql.services.interfaces.ICrudSubjectService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class SubjectMutationResolver implements GraphQLMutationResolver {

    private ICrudSubjectService iSubjectService;

    public Subject createSubject(final String name, String teacherName, String teacherSurname) {
        return iSubjectService.createSubject(name, teacherName, teacherSurname);
    }
}