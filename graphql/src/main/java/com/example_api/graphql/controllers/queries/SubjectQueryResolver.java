package com.example_api.graphql.controllers.queries;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import com.example_api.graphql.dtos.SubjectAndTeacherDTO;
import com.example_api.graphql.services.interfaces.ICrudSubjectService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@AllArgsConstructor
public class SubjectQueryResolver implements GraphQLQueryResolver {

    private ICrudSubjectService subjectService;

    public SubjectAndTeacherDTO getOneSubject(final Long id){
        return subjectService.findById(id);
    }

    public List<SubjectAndTeacherDTO> getSubjects(final int quantity){
        return subjectService.getAll(quantity);
    }

}

