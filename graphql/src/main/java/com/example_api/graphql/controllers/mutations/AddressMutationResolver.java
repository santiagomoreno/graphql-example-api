package com.example_api.graphql.controllers.mutations;

import com.coxautodev.graphql.tools.GraphQLMutationResolver;
import com.example_api.graphql.models.Address;
import com.example_api.graphql.services.interfaces.ICrudAddressService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;


@Component
@AllArgsConstructor
public class AddressMutationResolver implements GraphQLMutationResolver {

    private ICrudAddressService iCrudAddressService;

    public Address createAddress(final String street, Integer number, String neighborhood) {
        return iCrudAddressService.createAddress(street, number, neighborhood);
    }
}