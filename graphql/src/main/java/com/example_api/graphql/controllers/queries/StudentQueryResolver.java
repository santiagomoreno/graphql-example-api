package com.example_api.graphql.controllers.queries;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import com.example_api.graphql.models.Student;
import com.example_api.graphql.services.interfaces.students.ICrudStudentService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@AllArgsConstructor
public class StudentQueryResolver implements GraphQLQueryResolver {

    private ICrudStudentService studentService;

    public List<Student> getStudents(final int quantity){
        return studentService.findAll(quantity);
    }

    public Student getOneStudent(final Long id){
        return studentService.findById(id);
    }
}
