package com.example_api.graphql.controllers.mutations;

import com.coxautodev.graphql.tools.GraphQLMutationResolver;
import com.example_api.graphql.models.Course;
import com.example_api.graphql.services.interfaces.ICrudCourseService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class CourseMutationResolver implements GraphQLMutationResolver {

    private ICrudCourseService iCourseService;

    public Course createCourse(final Integer year) {
        return iCourseService.createCourse(year);
    }
}
